#include <stdio.h> //printf, scanf, fgets
#include <string.h> //strtok, strlen
#include <stdlib.h> //realloc, free, exit
#include <ctype.h> //isalpha, isdigit

#define DEBUG 1

int readline();
int parseelem(char *elem, double *d);
int parseop(char *elem, char *c);
int calculate(double *res, double *a, double *b, char operator);
int addtomem(double number);
double *getfrommem(char *c);
int issinglechar(char *c);
int isanumber(char *elem);
int isadouble(char *elem);
void debug(char *c);

void debug(char *c) {
	if (DEBUG) {
		printf("--> %s\n", c);
	}
}

double *mem;
int size;

int main(int argc, char *argv[]) {
	int i;
	double _tmp;
	for (i = 1; i < argc; i++) {
		if ((!parseelem(argv[i], &_tmp))) {
			addtomem(_tmp);
		} else {
			printf("\'%s\' is NAN, could not put in calculator memory.\n", argv[i]);
		}
	}

	while (readline());
}

int calculate(double *res, double *a, double *b, char operator) {
	switch (operator) {
		case '+':
		debug("operator is plus");
		*res = *a+*b;
		break;
		case '-':
		debug("operator is minus");
		*res = *a-*b;
		break;
		case '*':
		debug("operator is times");
		*res = *a*(*b);
		break;
		case '/':
		debug("operator is divide");
		*res = *a/(*b);
		break;
		default:
		return 1;
	}
	if (operator == '/' && *b == 0)
		return 1;
	return 0;
}

int readline() {
	char str[60];
	char *pch, operator;
	double one, two, result;

	printf("Eingabe > ");
	if (fgets(str, 60, stdin)) {
		if ( strcmp("quit\n", str) == 0) {
			return 0;
		} else {
			if (strcmp("\n", str) != 0) {
				pch = strtok(str, "\n");
				pch = strtok(str, " ");
				if (!parseelem(pch, &one)) {
					if ((pch = strtok(NULL, " ")) != NULL) {
						if (!parseop(pch, &operator)) {
							if ((pch = strtok(NULL, " ")) != NULL) {
								if (!parseelem(pch, &two)) {
									if (!calculate(&result, &one, &two, operator)) {
										addtomem(result);
										return 1;
									}
								}
							}
						}
					}
				}
				printf("Could not parse.\n");
			}
		}
	}
	return 1;
}

int issinglechar(char *elem) {
	debug("issinglechar?");
	if ((strlen(elem) == 1) && (isalpha(elem[0])))
		return 1;
	return 0;
}
int isanumber(char *elem) {
	debug("isanumber?");
	int i;
	for (i = 0; i < strlen(elem); i++) {
		if ((!(isdigit(elem[i]))) && (elem[i]!='.') && (elem[i]!='-') && (elem[i]!='+')) {
			return 0;
		}
	}
	return 1;
}
int isadouble(char *elem) {
	debug("isadouble?");
	int i;
	if (isanumber(elem)) {
		for (i = 0; i < strlen(elem); i++) {
			if (elem[i]=='.') {
				return 1;
			}
		}
	}
	return 0;
}

int parseelem(char *elem, double *d) {
	double *_tmpptr;
	if (issinglechar(elem)) {
		if ((_tmpptr = getfrommem(&elem[0]))) {
			memcpy(d, _tmpptr, sizeof(&d));
			return 0;
		}
	}
	if (isadouble(elem)) {
		double _tmp = atof(elem);
		memcpy(d, &_tmp, sizeof(&d));
		return 0;
	}
	if (isanumber(elem)) {
		double _tmp = atoi(elem);
		memcpy(d, &_tmp, sizeof(&d));
		return 0;
	}
	return 1;
}

int parseop(char *elem, char *operator) {
	if (strlen(elem) == 1) {
		*operator = *elem;
		return 0;
	}
	printf("Could not parse operator.\n");
	return 1;
}

double *getfrommem(char *c) {
	double *_ptr = NULL;
	int index = *c - '0' - 49;
	if (size > index) {
		_ptr = malloc(sizeof *_ptr);
		memcpy(_ptr, &mem[index], sizeof(*_ptr));
	}
	return _ptr;
}
int addtomem(double number) {
	if (size <= 26) {
		size++;
		double *_tmp = realloc(mem, size*sizeof(double));
		if (!_tmp) {
			fprintf(stderr, "ERROR: Couldn't realloc memory!\n");
			return 0;
		}
		mem = (double*)_tmp;
		mem[size-1] = number;
		char c = '0' + (size+48);
		printf("%c = %lf\n", c, number);
		return 1;
	}
	printf("Could not add '%lf': internal memory full.\n", number);
	return 0;
}
