#include <stdlib.h> //malloc
#include <string.h> //strlen
#include "atbash.h"

int atbash_encrypt(const char *plaintext, char **ciphertext)
{
	int length = strlen(plaintext);
	*ciphertext = (char*)malloc(length);
	int i;

	for (i = 0; i < length; i++) {
		(*ciphertext)[i] = 126 - (plaintext[i] - 32);
	}
	return 0;
}

int atbash_decrypt(const char *ciphertext, char **plaintext)
{
	int length = strlen(ciphertext);
	*plaintext = (char*)malloc(length);
	int i;

	for (i = 0; i < length; i++) {
		(*plaintext)[i] = 126 - ciphertext[i] + 32;
	}
	return 0;
}
