#ifndef __ATBASH_H__
#define __ATBASH_H__

/**
 * Verschluesselt einen uebergebenen Plaintext mit dem Atbash Cipher.
 *
 * @param plaintext Text, welcher verschluesselt werden soll.
 * @param ciphertext Pointer auf Pointer, Speicherbereich wird angefordert und
 * muss vom Aufrufer freigegeben werden.
 * @return -1 bei Fehler (z.B. Speicherplatz kann nicht angefordert werden),
 * ansonsten 0.
 */
int atbash_encrypt(const char *plaintext, char **ciphertext);

/**
 * Entschluesselt einen uebergebenen Ciphertext mit dem Atbash Cipher.
 *
 * @param ciphertext Text, welcher entschluesselt werden soll.
 * @param plaintext Pointer auf Pointer, Speicherbereich wird angefordert und
 * mus vom Aufrufer wieder freigegeben werden.
 * @return -1 bei Fehler (z.B. Speicherplatz kann nicht angefordert werden),
 * ansonsten 0.
 */
int atbash_decrypt(const char *ciphertext, char **plaintext);

#endif /* __ATBASH_H__ */

