#include <stdio.h>
#include <stdlib.h>

#include "atbash.h"
#include "vigenere.h"

int main(int argc, char **argv)
{
	char message_1[] = "Meet me at Midnight";
	char message_2[] = "security @ FHSTP";
	char atbash_ciphertext[] = "Z929*9~=22~S9%+~=0:~<),0~*69~zzzp";
	char vigenere_ciphertext[] = "C5.;c&4;5";

	char vigenere_key[] = "koancadlocevAcsEbOcOceejbennIrropduxUlArjEykEebrOgdarovOshej";

	char *result = NULL;

	if (atbash_encrypt(message_1, &result) == 0) {
		printf("[atbash] %s -> %s\n", message_1, result);
		free(result);
	}

	if (atbash_encrypt(message_2, &result) == 0) {
		printf("[atbash] %s -> %s\n", message_2, result);
		free(result);
	}

	if (atbash_decrypt(atbash_ciphertext, &result) == 0) {
		printf("[atbash] %s -> %s\n", atbash_ciphertext, result);
		free(result);
	}

	if (vigenere_encrypt(vigenere_key, message_1, &result) == 0) {
		printf("[vigenere] %s -> %s\n", message_1, result);
		free(result);
	}

	if (vigenere_encrypt(vigenere_key, message_2, &result) == 0) {
		printf("[vigenere] %s -> %s\n", message_2, result);
		free(result);
	}

	if (vigenere_decrypt(vigenere_key, vigenere_ciphertext, &result) == 0) {
		printf("[vigenere] %s -> %s\n", vigenere_ciphertext, result);
		free(result);
	}

	return 0;
}

