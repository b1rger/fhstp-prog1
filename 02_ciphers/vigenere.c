#include <stdlib.h> //malloc
#include <string.h> //strlen
#include <stdio.h>
#include "vigenere.h"

int vigenere_encrypt(const char *key, const char *plaintext, char **ciphertext)
{
	int length = strlen(plaintext);
	*ciphertext = (char*)malloc(length);
	int i, tmp;

	for (i = 0; i < length; i++) {
		tmp = plaintext[i] + key[i];
		tmp %= 127;
		if (tmp < 32)
			tmp = 127 - (32 - tmp);
		(*ciphertext)[i] = tmp; 
	}
	(*ciphertext)[length] = '\0';
	return 0;
}

int vigenere_decrypt(const char *key, const char *ciphertext, char **plaintext)
{
	int length = strlen(ciphertext);
	*plaintext = (char*)malloc(length);
	int i, tmp;

	for (i = 0; i < length; i++) {
		tmp = (ciphertext[i] - key[i]) + 127;
		tmp %= 127;
		if (tmp < 32)
			tmp += 32;
		(*plaintext)[i] = tmp;
	}
	(*plaintext)[length] = '\0';
	return 0;
}
