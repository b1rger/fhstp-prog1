#ifndef __VIGENERE_H__
#define __VIGENERE_H__

/**
 * Verschluesselt einen uebergebenen Plaintext mit dem Vigenere Cipher.
 *
 * @param key Schluessel, welcher fuer die Verschluesselung verwendet werden
 * soll.
 * @param plaintext Text, welcher verschluesselt werden soll.
 * @param ciphertext Pointer auf Pointer, Speicherbereich wird angefordert und
 * muss vom Aufrufer freigegeben werden.
 * @return -1 bei Fehler (z.B. Speicherplatz kann nicht angefordert werden),
 * ansonsten 0.
 */
int vigenere_encrypt(const char *key, const char *plaintext, char **ciphertext);

/**
 * Entschluesselt einen uebergebenen Ciphertext mit dem Vigenere Cipher.
 *
 * @param key Schluessel, welcher fuer die Entschluesselung verwendet werden
 * soll.
 * @param ciphertext Text, welcher verschluesselt werden soll.
 * @param plaintext Pointer auf Pointer, Speicherbereich wird angefordert und
 * muss vom Aufrufer freigegeben werden.
 * @return -1 bei Fehler (z.B. Speicherplatz kann nicht angefordert werden),
 * ansonsten 0.
 */
int vigenere_decrypt(const char *key, const char *ciphertext, char **plaintext);

#endif /* __VIGENERE_H__ */

