.PHONY: all ue1 ue2

all: ue1 ue2

bin:
	mkdir bin

ue1: ue1-1 ue1-2 ue1-3 ue1-4 ue1-5

ue1-1 ue1-2 ue1-3 ue1-4 ue1-5: bin
	gcc -Wall -o bin/$(@) $(subst -,/,$(@)).c -lm

ue2: ue2-1a ue2-1b ue2-2a ue2-2b ue2-3a ue2-3b ue2-4 ue2-5 ue2-6a ue2-6b ue2-7a ue2-7b

ue2-1a ue2-1b ue2-2a ue2-2b ue2-3a ue2-3b ue2-4 ue2-5 ue2-6a ue2-6b ue2-7a ue2-7b: bin
	gcc -Wall -o bin/$(@) $(subst -,/,$(@)).c
