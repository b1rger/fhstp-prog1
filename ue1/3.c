#include <stdio.h>
#include <math.h>

int main ()
{
	long r;

	printf("Bitte einen Radius eingeben: \n");
	scanf(" %ld", &r);

	printf("Der eingebene Radius ist: %ld \n", r);

	long volume;
	volume = 4/3 * M_PI * pow(r, 3);
	printf("Das Volumen ist: %ld \n", volume);

	long surface;
	surface = 4 * M_PI * pow(r, 2);
	printf("Die Oberflaeche ist: %ld \n", surface);

	return 0;
}
