#include <stdio.h>

int main ()
{
	int g;
	printf("Bitte Ganzzahl eingeben: ");
	scanf("%d", &g);

	printf("Zahl im ...\n");
	printf("\t Dezimalsystem: %d\n", g);
	printf("\t Oktalsystem: %.4o\n", g);
	printf("\t Hexadezimalsystem: %#x\n", g);

	return 0;
}
