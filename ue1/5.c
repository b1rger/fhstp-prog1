#include <stdio.h>

int main()
{
	char input[10];
	int i, s, p, array[10];
	printf("Bitte SVNR eingeben:\n");

	scanf("%s", input);
	for (i=0; i<10; i++) {
		/* If you're trying to convert a numerical char to an int, just
 		 * use character arithmetic to subtract the ASCII code */
		array[i] = input[i]-'0';
		//printf("array[%d] ist %d", i, array[i]);
	}

	s = (3*array[0]) + (7*array[1]) + (9*array[2]) + (5*array[4]) + (8*array[5]) + (4*array[6]) + (2*array[7]) + array[8] + (6*array[9]);
	printf("s ist %d\n", s); 
	p = s % 11;
	printf("Die Pruefsumme lautet %d\n", p);
}
