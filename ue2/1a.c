#include <stdio.h>

int main() {
int x = 2, y = 8;
/* die Ueberpruefung, ob der Wert von x 3 ist ergibt FALSE (=0);
 * die Zuweisung des Wertes 5 zu y wird nicht gemacht, da die Ueberpruefung
 * davor FALSE ist (if the result of the entire operation can be determined 
 * by the first argument, the second argument is not evaluated) */
printf("%d\n", (x == 3) && (y = 5));
/* x wird VOR der ausgabe inkrementiert; y wird NACH der ausgabe dekrementiert
 * also wird x auf 3 gesetzt und y bleibt vorerst noch 8, ist aber nach dem
 * printf dann 7 */
printf("%d %d\n", ++x, y--);
/* die Verneinung von 0 ist 1; die Verneinung von 1 ist 0 ;) */
printf("%d\n", !(!0));
}
