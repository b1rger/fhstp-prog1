#include <stdio.h>

int main() {
int a = 2, b = 3;
/* Der erste Teil des OR ist FALSE, weil erst nach dem Vergleich inkrementiert
 * wird
 * Weiters: "An assignment expression has the value of the left operand after
 * the assignment", somit ist der zweite Teil 0 (=FALSE)
 * der zweite Teil des OR wird ueberhaupt nur deswegen ausgefuehrt, weil durch
 * den ersten Teil noch nicht klar ist, was das Ergebniss ist (conditional OR: 
 * if the result of the entire operation can be determined by the first
 * argument, the second argument is not evaluated) */
printf("%d\n", (a++ == b) || (b -= 3));
/* 3, weil a vorher inkrementiert wurde; nach dieser anweiseung ist
 * der wert von a 4
 * b wurde vorher auf 0 gesetzt und wird nun vor der ausgabe dekrementiert */
printf("%d %d\n", a++, --b);
/* 27 verneint ist gleich 0; 0 verneint ist gleich 1 */
printf("%d\n", !(!27));
}
