/* TODO: beachten Sie auch die Einrückungen und Rundungen der 
 * Zahlen */
#include <stdio.h> //printf, scanf

int main() {
	int h, m, s;
	float martime, tlen, time;
	int perc;

	printf("Laenge der Trainingsstrecke (in Meter): ");
	scanf("%f", &tlen);

	printf("Zeit fuer die Trainingsstrecke (in hh:mm:ss): ");
	scanf("%d:%d:%d", &h, &m, &s);

	printf("Leistungsverlust bei Marathon (in %%): ");
	scanf("%d", &perc);

	printf("\n----------------------------------------");
        printf("----------------------------------------\n");

	printf("Geschaetzte Marathonzeit: ");
	time = h*3600+m*60+s;
	martime = ((time/tlen) * 1.06) * 42195;

	h = martime/3600;
	martime -= h * 3600;
	m = martime/60;
	martime -= m * 60;
	
	printf("%d Stunden, %d Minuten und %.0f Sekunden fuer 42,195km\n", h, m, martime);
}
