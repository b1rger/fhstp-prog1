#include <stdio.h>

int main() {
	int azs, nzs, st;
	float ggb, preis, netto, steuer;
	printf("Alter Zaehlerstand (in kWh): ");
	scanf("%i", &azs);
	printf("Neuer Zaehlerstand (in kWh): ");
	scanf("%i", &nzs);
	printf("Grundgebuehr (in EUR): ");
	scanf("%f", &ggb);
	printf("Preis pro kWh (in EUR): ");
	scanf("%f", &preis);
	printf("Umsatzsteuer (in %%): ");
	scanf("%i", &st);

	printf("\n----------------------------------------");
        printf("----------------------------------------\n");

	// (((neuer zaehler - alter zaehler) * preis ) + ggb)
	netto = ((((float)(nzs - azs)) * preis) + (float)ggb);
	// (netto * (1 + steuer / 100)) - netto
	steuer = (netto * (1+((float)st/100))) - netto;
	
	printf("Rechnungsbetrag inkl. USt: %.2f EUR, davon %d%% USt: %.2f EUR\n", netto + steuer, st, steuer);
}
