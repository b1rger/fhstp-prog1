#include <stdio.h> //printf, scanf

int main() {
	int h, m, s;
	float martime, tlen, time;
	int perc;

	printf("Laenge der Trainingsstrecke (in Meter): ");
	if (((scanf("%f", &tlen)) != 1) | (tlen < 1)){
		printf("Falsche Eingabe!\n");
		return 1;
	}

	printf("Zeit fuer die Trainingsstrecke (in hh:mm:ss): ");
	if ((scanf("%d:%d:%d", &h, &m, &s) != 3) | (m > 59) | (s > 59)) {
		printf("Falsche Eingabe!\n");
		return 1;
	}

	printf("Leistungsverlust bei Marathon (in %%): ");
	if ((scanf("%d", &perc) != 1) | (perc > 100) ) {
		printf("Falsche Eingabe!\n");
		return 1;
	}

	printf("\n----------------------------------------");
        printf("----------------------------------------\n");

	printf("Geschaetzte Marathonzeit: ");
	time = h*3600+m*60+s;
	martime = ((time/tlen) * 1.06) * 42195;

	h = martime/3600;
	martime -= h * 3600;
	m = martime/60;
	martime -= m * 60;
	
	printf("%d Stunden, %d Minuten und %.0f Sekunden fuer 42,195km\n", h, m, martime);
}
