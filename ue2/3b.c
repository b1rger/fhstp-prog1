#include <stdio.h>
/* there is still one nasty bug: "in C, if you will use
 * scanf("%d",&num) and define num as of int type then by default it
 * will pick just the num before dot. incase u try to pick another
 * num after that then you will encounter an error because this time
 * you will get the number after dot" */

int main() {
	int azs, nzs, st;
	float ggb, preis, netto, steuer;
	printf("Alter Zaehlerstand (in kWh): ");
	if ((scanf("%d", &azs)) != 1) {
		printf("Falsche Eingabe!\n");
		return 1;
	}
	printf("Neuer Zaehlerstand (in kWh): ");
	if ((scanf("%d", &nzs)) != 1) {
		printf("Falsche Eingabe!\n");
		return 1;
	}
	printf("Grundgebuehr (in EUR): ");
	if ((scanf("%f", &ggb)) != 1) {
		printf("Falsche Eingabe!\n");
		return 1;
	}
	printf("Preis pro kWh (in EUR): ");
	if ((scanf("%f", &preis)) !=1) {
		printf("Falsche Eingabe!\n");
		return 1;
	}
	printf("Umsatzsteuer (in %%): ");
	if (((scanf("%d", &st)) !=1) | (st > 100)) {
		printf("Falsche Eingabe!\n");
		return 1;
	}

	printf("\n----------------------------------------");
        printf("----------------------------------------\n");

	// (((neuer zaehler - alter zaehler) * preis ) + ggb)
	netto = ((((float)(nzs - azs)) * preis) + (float)ggb);
	// (netto * (1 + steuer / 100)) - netto
	steuer = (netto * (1+((float)st/100))) - netto;
	
	printf("Rechnungsbetrag inkl. USt: %.2f EUR, davon %d%% USt: %.2f EUR\n", netto + steuer, st, steuer);
}
