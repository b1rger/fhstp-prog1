#include <stdio.h>

int main() {
	int a, b, c;
	printf("Zahlen eingeben: ");
	scanf("%i %i %i", &a, &b, &c);

	if (a>b) {
		if (a>c) {
			printf("1: %d\n", a);
			if (b>c) {
				printf("2: %d\n", b);
				printf("3: %d\n", c);
			} else {
				printf("2: %d\n", c);
				printf("3: %d\n", b);
			}
		} else { // a < c
			printf("1: %d\n", c);
			printf("2: %d\n", a);
			printf("3: %d\n", b);
		}
	} else { // a < b
		if (b>c) {
			printf("1: %d\n", b);
			if (c>a) {
				printf("2: %d\n", c);
				printf("3: %d\n", a);
			} else {
				printf("2: %d\n", a);
				printf("3: %d\n", c);
			}
		} else {
			printf("1: %d\n", c);
			printf("2: %d\n", b);
			printf("2: %d\n", a);
		}
	}
}
