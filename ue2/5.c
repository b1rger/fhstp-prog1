#include <stdio.h>
#include <stdlib.h> //rand(), srand()
#include <time.h> //time()
#define PLUS 0
#define MINUS 1
#define NONE 2

int main() {
	int r, apos;
	int plusminusnone = NONE;
	char array[] = {'A', 'B', 'C', 'D'};

	srand(time(NULL));
	r = rand()%40;
	r+=60;

	//printf("Meine Zufallszahl %d\n", r);
	if (r%10 > 8) {
		plusminusnone = PLUS;
	} else if (r%10 < 2) {
		plusminusnone = MINUS;
	}
	if (r<100) {
		apos = 0;
	}
	if (r<90) {
		apos = 1;
	}
	if (r<80) {
		apos = 2;
	}
	if (r<70) {
		apos = 3;
	}
	if (plusminusnone == PLUS) {
		printf("%c+\n", array[apos]);
	} else if (plusminusnone == MINUS) {
		printf("%c-\n", array[apos]);
	} else {
		printf("%c\n", array[apos]);
	}
	return 0;
}
