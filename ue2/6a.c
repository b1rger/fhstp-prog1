#include <stdio.h>

int main() {
	/* eine zuweisung hat den (rueckgabe)wert der linken 
	 * variable b-- ist gleichbedeutend mit b = b-1; dh. die 
	 * schleife wird 4x durchgefuehrt, bis b gleich 0 ist;
	 * i ist am ende dann 5+4+3+2+1 = 15 */
	int b = 5, i = 0;
	do {
		i += b;
	} while (b--);

	printf("%d", i);
}
