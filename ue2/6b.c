#include <stdio.h>

int main() {
	int i, x =0;

	/* i wird jeweils verdoppelt, und zwar solange bis es
	 * groesser als 10 ist. also 1, 2, 4, 8, 16. bei 16 wird
	 * das schleifenstatement schon nicht mehr ausgefuehrt.
	 * x wird also 4x inkrementiert, auf 4 */
	for (i = 1; i < 10; i *= 2) {
		x++;
		printf("%d ", x);
	}
}
