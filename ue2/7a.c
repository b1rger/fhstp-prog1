#include <stdio.h>

int main() {
	int voka, voke, voki, voko, voku;
	voka = voke = voki = voko = voku = 0;
	char c;
	while ((c = getchar()) != EOF) {
		switch (c) {
			case 'a': case 'A': 
				voka++;
				break;
			case 'e': case 'E':
				voke++;
				break;
			case 'i': case 'I': 
				voki++;
				break;
			case 'o': case 'O':
				voko++;
				break;
			case 'u': case 'U':
				voku++;
				break;
		}
	}
	printf("aA: %d, eE: %d, iI: %d, oO: %d, uU: %d\n", voka, voke, voki, voko, voku);
	return 0;
}
