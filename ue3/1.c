#include <stdio.h>
typedef enum { false, true } bool;

int main() {
	bool abbruch = false;
	int zahl, pos, neg;
	zahl = pos = neg = 0;

	while (!abbruch) {
		printf("-> ");
		if (scanf("%d", &zahl) == 1) {
			if (zahl < 0) {
				neg++;
			}
			if (zahl > 0) {
				pos++;
			}
			if (zahl == 0) {
				abbruch = true;
			}
		} else {
			abbruch = true;
		}
	}
	printf("%d positive und %d negative Zahlen gelesen\n", pos, neg);
	return 0;
}
