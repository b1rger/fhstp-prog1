#include <stdio.h>

int main() {
	int d, m, y;
	printf("-> ");
	scanf("%d.%d.%d", &d, &m, &y);
	
	if (check_date(d, m, y) == 0) {
		printf("Datum korrekt!\n");
	} else {
		printf("Datum falsch!\n");
	}
}


/* wir gehen davon aus, dass die eingabe falsch ist;
 * das jahr wird hier gar nicht ueberprueft (koennte noch als
 * oberstes statement eingefuegt werden)
 * nur wenn die das monat richtig ist und der tag dazu passt,
 * wird 0 als wert zurueckgegeben */
int check_date(int d, int m, int y) {
	switch (m) {
		case 1: case 3: case 5: case 7: case 8: case 10: case 12:
		if (d<=31)
			return 0;
		break;
		case 4: case 6: case 9: case 11:
		if (d<=30)
			return 0;
		break;
		case 2:
		if (d<=28)
			return 0;
		break;
	}
	
	return -1;
}
