#include <stdio.h> //printf, scanf
#include <math.h> //abs, sqrt

double triangle_area(double a, double b, double c);
double perimeter(double a, double b, double c);
int is_triangle(double a, double b, double c);

int main() {
	double a, b, c;

	scanf("%lf %lf %lf", &a, &b, &c);
	printf("a: %lf, b: %lf, c: %lf\n", a, b, c);

	if (is_triangle(a, b, c) != 0) {
		printf("eine Fehlermeldung\n");
		return 1;
	} else {
		printf("%lf\n", triangle_area(a, b, c));
		return 0;
	}
}

int is_triangle(double a, double b, double c) {
	if (a>0 && b>0 && c>0)
		if ((abs(a-b) <= c) && (c <= a+b))
			return 0;
	return -1;
}

double triangle_area(double a, double b, double c) {
	double s = perimeter(a, b, c)/2;

	return sqrt(s*(s-a)*(s-b)*(s-c));
}

double perimeter(double a, double b, double c) {
	return a + b + c;
}
