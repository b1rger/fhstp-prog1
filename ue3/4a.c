#include <stdio.h> //scanf, fprintf
#include <math.h> //M_PI

int check_conditions(double r1, double r2);
double area(double r1, double r2);

int main() {
	double r1, r2;

	scanf("%lf %lf", &r1, &r2);
	if ((check_conditions(r1, r2)) == 0) {
		printf("Area: %lf\n", area(r1, r2));
	} else {
		printf("Conditions not met.\n");
		return 1;
	}
	return 0;
}

int check_conditions(double r1, double r2) {
	if (r1>r2)
		if (r1>0 && r2>0)
			return 0;
	return -1;
}

double area(double r1, double r2) {
	return M_PI*(pow(r1, 2) - pow(r2, 2));
}
