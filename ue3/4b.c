#include <stdio.h>
#include <math.h>

int ggt(int a, int b);
int kgv(int a, int b);

int ggt(int a, int b) {
	int h;
	if (a <=0 || b<=0) {
		return -1;
	}
	while (b != 0) {
		h = a % b;
		a = b;
		b = h;
	}
	return a;
}

int kgv(int a, int b) {
	if (a <=0 || b<=0) {
		return -1;
	}
	return ((a*b)/ggt(a, b));
}

int main() {
	int a, b;

	scanf("%d %d", &a, &b);

	if (ggt(a, b) != -1) {
		printf("ggt: %d\n", ggt(a, b));
	} else {
		printf("a or b less than or equal to zero.\n");
	}
	if (kgv(a, b) != -1) {
		printf("kgv: %d\n", kgv(a, b));
	} else {
		printf("a or be less than or equal to zero.\n");
	}
	return 0;
}
