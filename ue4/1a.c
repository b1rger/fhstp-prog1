#include <stdio.h>

int main()
{
	int i = 0, j = 25, k;
	//*pi zeigt auf nix;
	//*pj zeigt auf speicherbereich wo auch j hinzeigt
	//*pk zeigt auf den zpeicherbereich wo k hinzeigt
	int *pi, *pj = &j, *pk = &k;
	// 25+5 -> 30
	*pj = j + 5;
	// i = 35
	i = *pj + 5;
	// pk zeigt jetzt auf 62 (27+35)
	*pk = 27 + ((i == 0) ? 3 : i);
	// pi zeigt auf &j
	pi = pj;
	// pi zeigt auf 35 + 30, dh dass auch pj dorthin zeigt
	// dh dass auch j dorthinzeigt
	*pi = i + j;
	printf("%d %d %d\n", i, j, *pk);
}
