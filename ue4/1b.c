#include <stdio.h>

int main()
{
	int a, b, c, d;
	int *f, *g, *h, *i;
	a=5; b=8; c=19; d=34;
	//f=>34, g=>8, h=>19, i=>5
	f=&d; g=&b; h=&c; i=&a;
	//f zeigt auf 35, d ist dadurch auch 35
	*f = *f+1;
	// i zeigt auf 8
	i = g;
	// g zeigt auf 68
	*g = d*2;
	// 35, 70, 19, 70
	printf("%d %d %d %d\n", *f, *g, *h, *i);
}
