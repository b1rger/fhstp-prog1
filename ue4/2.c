#include <stdio.h>

int f(int *k, int j)
{
	//neues m mit dem wert von j
	int m = j;
	//j kriegt neue wert von m + dem wert von da wo k hinzeigt
	j = m + (*k);
	//da wo k hinzeigt steht jetzt j^2
	*k = j * j;
	printf("%d %d\n", *k, j);
	return j;
}
int main()
{
	int k = 1, m = 2;
	// hier wird k 9
	f(&k, m);
	// 9, 2
	printf("%d %d\n", k, m);
	// m = m + k -> m = 11
	// k = k - 1 -> k = 8
	m += k--;
	// hier wird m = 19*19
	f(&m, k);
	// 8, 19*19
	printf("%d %d\n", k, m);
	return 0;
}
