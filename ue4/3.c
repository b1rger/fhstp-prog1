#include <stdio.h>

int determine_time(unsigned long seconds, int *h, int *m, int *s);
const int secmax = 86399;

int main()
{
	int seconds;
	int h, m, s;
	printf("Sekunden eingeben: ");
	scanf("%d", &seconds);
	if (determine_time(seconds, &h, &m, &s)) {
		printf("Eingabe entspricht der Uhrzeit: %d:%d:%d\n", h, m, s);
	} else {
		printf("Falsche eingabe, Programmabbruch\n");
	}
}

int determine_time(unsigned long seconds, int *h, int *m, int *s)
{
	if (0 < seconds && seconds < secmax) {
		*h = seconds / 3600;
		*m = (seconds % 3600) / 60;
		*s = ((seconds % 3600) % 60);
		return 1;
	} else {
		return 0;
	}
}
