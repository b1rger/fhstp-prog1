#include <stdio.h>

int increase(double *value, double wert);
int print(double array[100]);

int main ()
{
	double array[100];
	double wert = 23;
	int i;

	for (i = 0; i < 100; i++) {
		array[i] = i + 1.0;
	}

	for (i = 90; i < 100; i++) {
		increase(&array[i], wert);
	}
	print(array);
}

int increase(double *value, double wert) {
	*value += wert;
}

int print(double array[100]) {
	int i;
	for (i = 0; i < 100; i++) {
		printf("[%d] = %lf\n", i, array[i]);
	}
}
