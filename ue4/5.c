#include <stdio.h>
#include <stdlib.h>
#include <time.h> //time()

int betrag(double *array);
double summe(double *array);
int print(double *array);

int main()
{
	double array[100];
	int i;
	double r;

	for (i = 0; i < 100; i++) {
		srand(time(NULL)+i);
		r = rand() % 200;
		array[i] = r - 100.0;
	}
	print(array);
	printf("%d Werte geaendert.\n", betrag(array));
	print(array);
	printf("%lf ist die Summer der Werte.\n", summe(array));
}

double summe(double *array)
{
	double sum = 0;
	int i;
	for (i = 0; i < 100; i++) {
		sum += array[i];
	}
	return sum;
}

int betrag(double *array) {
	int amount = 0, i;
	for (i = 0; i < 100; i++) {
		if (array[i] < 0) {
			amount++;
			array[i] *= -1;
		}
	}
	return amount;
}

int print(double *array) {
	int i;
	for (i = 0; i < 100; i++) {
		printf("[%d] %lf\n", i, array[i]);
	}
}
