#include <stdio.h>
#include <stdlib.h>
#include <time.h> //time()

int print(int *array);
int selsort(int *array);

int main()
{
	int array[100];
	int i;

	for (i = 0; i < 100; i++) {
		srand(time(NULL)+i);
		array[i] = rand() % 10000;
	}

	print(array);
	selsort(array);
	print(array);

}

int print(int *array)
{
	int i;
	for (i = 0; i < 100; i++) {
		printf("[%d] %d\n", i, array[i]);
	}
}

int selsort(int *array)
{
	int i,j, imin, tmp;

	for (j = 0; j < 100; j++) {
		imin = j;
		for (i = j+1; i < 100; i++) {
			if (array[i] < array[imin]) {
				imin = i;
			}
		}
		if (imin != j) {
			tmp = array[j];
			array[j] = array[imin];
			array[imin] = tmp;
		}
	}
}
