#include <stdio.h>
#include <string.h> //strlen

char *delete_char(char *string, char a);

int main()
{
	char foo[] = "this is just a test";
	char bar[] = "All work and no play makes Jack a dull boy.";
	printf("%s\n", foo);
	printf("%s\n", delete_char(foo, 'i'));
	printf("%s\n", bar);
	printf("%s\n", delete_char(bar, 'o'));

}

char *delete_char(char *string, char a)
{
	int i, j;
	int length = strlen(string);
	for (i = 0; i < length; i++) {
		if (string[i] == a) {
			for (j = i; j < length; j++) {
				string[j] = string[j+1];
			}
		}
	}
	return string;
}
