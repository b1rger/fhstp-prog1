#include <stdio.h>
#include <string.h> //strlen
#include <ctype.h>  //isdigit
#include <stdlib.h> //atoi

int teststr(char *str);
int formstr(char *str, int *vw, int *praefix, int *suffix);

int main()
{
	char str[80];
	int vw, praefix, suffix;

	printf("Enter the number:\n");
	gets(str);

	if (!teststr(str)) {
		printf("String not valid.\n");
	} else {
		formstr(str, &vw, &praefix, &suffix);
		printf("(%d)%d-%d\n", vw, praefix, suffix);
	}

}

int teststr(char *str) {
	int i;
	char c;

	if (strlen(str) != 10)
		return 0;

	for (i = 0; i < strlen(str); i++) {
		if (!isdigit(str[i])) {
			return 0;
		} else {
			if (i == 0) {
				c = str[i];
				if (atoi(&c) == 1 || atoi(&c) == 0) {
					return 0;
				}
			}
		}
	}
	return 1;
}

int formstr(char *str, int *vw, int *praefix, int *suffix)
{
	char tmpvw[3], tmppraefix[3], tmpsuffix[4];
	strncpy(tmpvw, str, 3);
	strncpy(tmppraefix, str+3, 3);
	strncpy(tmpsuffix, str+6, 4);

	*vw = atoi(tmpvw);
	*praefix = atoi(tmppraefix);
	*suffix = atoi(tmpsuffix);
}
