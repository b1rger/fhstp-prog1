#include <stdio.h>

char *get_arg(int argc, char **argv, char o);

int main(int argc, char **argv)
{
	char o;
	char *arg;

	printf("Zu suchende Option: ");
	o = getchar();

	if ((arg = get_arg(argc, argv, o))!=NULL) {
		printf("\nArgument zu -%c: %s\n", o, arg);
	} else {
		printf("\nkein Argument zu -%c gefunden\n", o);
	}
}

char *get_arg(int argc, char **argv, char o)
{
	int i;
	for (i = 1; i < argc; i++) {
		if (argv[i][1] == o) {
			return argv[i+1];
		}
	}
	return NULL;
}
