#include <stdio.h>
#include <stdlib.h> //atoi
#include <string.h> //strlen

int strtodec(const char *str, int *num);

int main(int argc, char *argv[])
{
	int num1, num2;
	double erg;
	if (argc != 4) {
		printf("Falsche Anzahl an Parametern.\n");
		return 1;
	}

	if (strtodec(argv[1], &num1) && strtodec(argv[3], &num2)) {
		switch (argv[2][0]) {
			case '+':
			erg = num1 + num2;
			printf ("%d + %d", num1, num2);
			break;
			case '-':
			erg = num1 - num2;
			printf ("%d - %d", num1, num2);
			break;
			case 'x':
			erg = (double)num1 * (double)num2;
			printf ("%d x %d", num1, num2);
			break;
			case '/':
			erg = (double)num1 / (double)num2;
			printf ("%d / %d", num1, num2);
			break;
			default:
			printf("Operator nicht erkannt.\n");
			return 1;
		}
		printf(" = %.2f\n", erg);
	} else {
		printf("Konnte Argument nicht parsen.\n");
		return 1;
	}
}

int strtodec(const char *str, int *num) {
	int i;
	for (i = 0; i < strlen(str); i++) {
		if (str[i] < '0' && '9' < str[i]) {
			return 0;
		}
	}
	*num = atoi(str);
	return 1;
}
