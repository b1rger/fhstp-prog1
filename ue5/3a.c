#include <stdio.h>
#include <stdlib.h> //atoi

double *geore(int n);

int main(int argc, char* argv[])
{
	int n, i;
	n = atoi(argv[1]);
	double *array = NULL;

	array = geore(n);

	for (i = 0; i < n; i++) {
		printf("[%d] %.2f\n", i, array[i]);
	}
	free(array);
}

double *geore(int n) {
	int i = 2, count;
	double *array = malloc (n*sizeof(double));
	array[0] = 2; 

	for (count = 0; count < n; count++) {
		array[count+1] = array[count]*3;
	}
	return array;
}
