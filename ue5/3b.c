#include <stdio.h>
#include <stdlib.h> //atoi

double *fibb(int n);

int main(int argc, char* argv[])
{
	int n, i;
	n = atoi(argv[1]);
	double *array = NULL;

	array = fibb(n);

	for (i = 0; i < n; i++) {
		printf("[%d] %.2f\n", i, array[i]);
	}
	free(array);
}

double *fibb(int n) {
	int i = 2, count;
	double *array = malloc (n*sizeof(double));
	array[0] = 1; 
	array[1] = 1;

	for (count = 2; count < n; count++) {
		array[count] = array[count-2]+array[count-1];
	}
	return array;
}
