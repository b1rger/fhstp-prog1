#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *insert(char *str, size_t pos, const char *ins);

int main(int argc, char *argv[])
{
	char *str = argv[1];
	const char *ins = argv[2];
	char c;
	int pos;

	//printf("%s into %s\n", ins, str);
	printf("Position: ");
	c = getchar();
	pos = atoi(&c);

	char *erg = insert(str, pos, ins);
	if (erg != NULL) {
		printf("%s\n", erg);
	} else {
		printf("Error: erg is NULL\n");
	}
}

char *insert(char *str, size_t pos, const char *ins)
{
	int strl = strlen(str);
	int insl = strlen(ins);
	char *tmp = NULL;
	if (pos <= strl) {
		tmp = malloc(strl+insl);

		strncpy(tmp, str, pos-1);
		strncpy(tmp + pos - 1, ins, insl);
		strncpy(tmp + pos - 1 + insl, str + pos - 1, strl - pos + 1);
	}
	return tmp;
}
