#include <stdio.h>

struct complex_number {
	double imagt;
	double realt;
};

void output(const struct complex_number *number);
struct complex_number add(struct complex_number *one, struct complex_number *two);
struct complex_number sub(struct complex_number *one, struct complex_number *two);
struct complex_number mul(struct complex_number *one, struct complex_number *two);
struct complex_number div(struct complex_number *one, struct complex_number *two);
struct complex_number inv(struct complex_number *number);

int main(int argc, char* argv[])
{
	struct complex_number one, two, three, four, five, six;
	one.realt = 3;
	one.imagt = 2;
	two.realt = 5;
	two.imagt = 5;
	three.realt = 3;
	three.imagt = 5;
	four.realt = 4;
	four.imagt = 11;
	five.realt = 2;
	five.imagt = 5;
	six.realt = 3;
	six.imagt = 7;


	output(&one);
	output(&two);
	output(&three);
	output(&four);
	output(&five);
	output(&six);
	printf("-----\n");

	struct complex_number tmp;
	tmp = add(&one, &two);
	output(&tmp);
	tmp = sub(&two, &one);
	output(&tmp);
	tmp = mul(&three, &four);
	output(&tmp);
	tmp = div(&five, &six);
	output(&tmp);


}

struct complex_number add(struct complex_number *one, struct complex_number *two)
{
	struct complex_number tmp;
	tmp.realt = one->realt+two->realt;
	tmp.imagt = one->imagt+two->imagt;
	return tmp;
}

struct complex_number sub(struct complex_number *one, struct complex_number *two)
{
	struct complex_number tmp;
	tmp.realt = one->realt-two->realt;
	tmp.imagt = one->imagt-two->imagt;
	return tmp;
}

struct complex_number mul(struct complex_number *one, struct complex_number *two)
{
	struct complex_number tmp;
	tmp.realt = one->realt*two->realt - one->imagt * two->imagt;
	tmp.imagt = one->realt*two->imagt + one->imagt * two->realt;
	return tmp;
}

struct complex_number div(struct complex_number *one, struct complex_number *two)
{
	struct complex_number tmp, nenner, teiler, invteiler;
	invteiler = inv(two);
	nenner = mul(one, &invteiler);
	teiler = mul(two, &invteiler);
	tmp.realt = nenner.realt / teiler.realt;
	tmp.imagt = nenner.imagt / teiler.imagt;
	return tmp;
}

struct complex_number inv(struct complex_number *number)
{
	struct complex_number tmp;
	tmp.realt = number->realt;
	tmp.imagt = number->imagt*-1;
	return tmp;
}

void output(const struct complex_number *number)
{
	if (number->imagt > 0) {
		printf("%.2f+%.2fi\n", number->realt, number->imagt);
	} else {
		printf("%.2f%.2fi\n", number->realt, number->imagt);
	}
}
